package rf.androidovshchik.application;

import android.os.StrictMode;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import rf.androidovshchik.application.di.components.DaggerAppComponent;
import rf.androidovshchik.application.di.components.DaggerRemoteComponent;
import rf.androidovshchik.application.remote.Client;
import timber.log.Timber;

public class Application extends DaggerApplication {

    public Client client;

    @Override
    public void onCreate() {
        super.onCreate();
        client = DaggerRemoteComponent.create()
                .client();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyDialog()
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
        }
    }

    @Override
    protected AndroidInjector<Application> applicationInjector() {
        return DaggerAppComponent
                .builder()
                .create(this);
    }
}