package rf.androidovshchik.application.remote;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.AsyncSubject;
import rf.androidovshchik.application.models.Album;
import rf.androidovshchik.application.models.Photo;
import rf.androidovshchik.application.models.Post;
import rf.androidovshchik.application.models.user.User;

@Singleton
public class Client {

    private final Api api;

    private AsyncSubject<ArrayList<User>> usersSubject;
    private AsyncSubject<ArrayList<Album>> albumsSubject;
    private AsyncSubject<ArrayList<Post>> postsSubject;
    private AsyncSubject<ArrayList<Photo>> photosSubject;

    private int albumsId = 0;
    private int postsId = 0;
    private int photosId = 0;

    @Inject
    public Client(Api api) {
        this.api = api;
    }

    public Observable<ArrayList<User>> getUsers() {
        if (usersSubject == null) {
            usersSubject = AsyncSubject.create();
            api.getUsers()
                    .subscribeOn(Schedulers.io())
                    .subscribe(usersSubject);
        }
        return usersSubject;
    }

    public Observable<ArrayList<Album>> getAlbums(int id) {
        if (albumsId != id) {
            albumsId = id;
            albumsSubject = null;
        }
        if (albumsSubject == null) {
            albumsSubject = AsyncSubject.create();
            api.getAlbums(id)
                    .subscribeOn(Schedulers.io())
                    .subscribe(albumsSubject);
        }
        return albumsSubject;
    }

    public Observable<ArrayList<Post>> getPosts(int id) {
        if (postsId != id) {
            postsId = id;
            postsSubject = null;
        }
        if (postsSubject == null) {
            postsSubject = AsyncSubject.create();
            api.getPosts(id)
                    .subscribeOn(Schedulers.io())
                    .subscribe(postsSubject);
        }
        return postsSubject;
    }

    public Observable<ArrayList<Photo>> getPhotos(int id) {
        if (photosId != id) {
            photosId = id;
            photosSubject = null;
        }
        if (photosSubject == null) {
            photosSubject = AsyncSubject.create();
            api.getPhotos(id)
                    .subscribeOn(Schedulers.io())
                    .subscribe(photosSubject);
        }
        return photosSubject;
    }
}