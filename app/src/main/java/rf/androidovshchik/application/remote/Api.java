package rf.androidovshchik.application.remote;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rf.androidovshchik.application.models.Album;
import rf.androidovshchik.application.models.Photo;
import rf.androidovshchik.application.models.Post;
import rf.androidovshchik.application.models.user.User;

public interface Api {

    String ENDPOINT = "https://jsonplaceholder.typicode.com";

    @GET("/users")
    Observable<ArrayList<User>> getUsers();

    @GET("/albums")
    Observable<ArrayList<Album>> getAlbums(@Query("userId") int userId);

    @GET("/photos")
    Observable<ArrayList<Photo>> getPhotos(@Query("albumId") int albumId);

    @GET("/posts")
    Observable<ArrayList<Post>> getPosts(@Query("userId") int userId);
}