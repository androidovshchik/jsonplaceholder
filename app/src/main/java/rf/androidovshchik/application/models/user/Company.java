package rf.androidovshchik.application.models.user;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import rf.androidovshchik.application.models.Model;

@Parcel
public class Company extends Model {

    @SerializedName("name")
    public String name;

    @SerializedName("catchPhrase")
    public String catchPhrase;

    @SerializedName("bs")
    public String bs;
}