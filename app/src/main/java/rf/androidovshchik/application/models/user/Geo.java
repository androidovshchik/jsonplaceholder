package rf.androidovshchik.application.models.user;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import rf.androidovshchik.application.models.Model;

@Parcel
public class Geo extends Model {

    @SerializedName("lat")
    public float lat;

    @SerializedName("lng")
    public float lng;
}