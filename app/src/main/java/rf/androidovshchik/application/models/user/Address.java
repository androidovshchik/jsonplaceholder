package rf.androidovshchik.application.models.user;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import rf.androidovshchik.application.models.Model;

@Parcel
public class Address extends Model {

    @SerializedName("street")
    public String street;

    @SerializedName("suite")
    public String suite;

    @SerializedName("city")
    public String city;

    @SerializedName("zipcode")
    public String zipCode;

    @SerializedName("geo")
    public Geo geo;
}