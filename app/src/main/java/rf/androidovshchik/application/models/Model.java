package rf.androidovshchik.application.models;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import timber.log.Timber;

public class Model {

    public String getFullInfo(Class clss) {
        StringBuilder builder = new StringBuilder();
        try {
            Field[] fields = clss.getFields();
            for (Field field: fields) {
                if (Modifier.isFinal(field.getModifiers()) || field.getName().equals("$change")) {
                    continue;
                }
                Object value = field.get(this);
                if (Model.class.isAssignableFrom(field.getType())) {
                    builder.append(((Model) value).getFullInfo(field.getType()));
                } else {
                    builder.append(field.getName());
                    builder.append(": ");
                    builder.append(value);
                }
                builder.append("\n");
            }
        } catch (IllegalAccessException e) {
            Timber.e(e);
        }
        return builder.toString().trim();
    }
}
