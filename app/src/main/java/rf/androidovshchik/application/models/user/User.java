package rf.androidovshchik.application.models.user;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import rf.androidovshchik.application.models.Model;

@Parcel
public class User extends Model {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("username")
    public String username;

    @SerializedName("email")
    public String email;

    @SerializedName("address")
    public Address address;

    @SerializedName("phone")
    public String phone;

    @SerializedName("website")
    public String website;

    @SerializedName("company")
    public Company company;

    public String getPartInfo() {
        return String.format("%s <%s> %s", name, email, address.city);
    }
}