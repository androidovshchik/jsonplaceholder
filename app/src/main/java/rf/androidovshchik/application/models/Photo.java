package rf.androidovshchik.application.models;

import com.google.gson.annotations.SerializedName;

public class Photo {

    @SerializedName("albumId")
    public int albumId;

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("url")
    public String url;

    @SerializedName("thumbnailUrl")
    public String thumbnailUrl;
}