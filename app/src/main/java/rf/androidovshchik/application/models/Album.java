package rf.androidovshchik.application.models;

import com.google.gson.annotations.SerializedName;

public class Album {

    @SerializedName("userId")
    public int userId;

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;
}