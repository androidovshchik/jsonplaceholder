package rf.androidovshchik.application.ui.details.albums;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import rf.androidovshchik.application.R;
import rf.androidovshchik.application.models.Album;
import rf.androidovshchik.application.ui.base.BaseFragment;
import timber.log.Timber;

public class AlbumsFragment extends BaseFragment<AlbumsAdapter> {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public static AlbumsFragment newInstance(int id) {
        AlbumsFragment fragment = new AlbumsFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_albums, container, false);
        unbinder = ButterKnife.bind(this, view);
        LinearLayoutManager layoutManager = getLayoutManager();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),
                layoutManager.getOrientation()));
        recyclerView.setAdapter(adapter);
        fetchData();
        return view;
    }

    @Override
    protected void fetchData() {
        disposables.add(getClient().getAlbums(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new AlbumsSubscriber()));
    }

    private class AlbumsSubscriber extends DisposableObserver<ArrayList<Album>> {

        @Override
        public void onNext(ArrayList<Album> albums) {
            adapter.setItems(albums);
        }

        @Override
        public void onComplete() {}

        @Override
        public void onError(Throwable e) {
            Timber.e(e);
        }
    }
}