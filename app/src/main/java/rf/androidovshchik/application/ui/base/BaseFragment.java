package rf.androidovshchik.application.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;

import javax.inject.Inject;

import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;
import io.reactivex.disposables.CompositeDisposable;
import rf.androidovshchik.application.Application;
import rf.androidovshchik.application.remote.Client;

public abstract class BaseFragment<T extends BaseAdapter> extends Fragment {

    public static final String EXTRA_ID = "id";

    protected int id;

    @Inject
    protected T adapter;

    @Nullable
    protected Unbinder unbinder;

    protected CompositeDisposable disposables = new CompositeDisposable();

    @Override
    @SuppressWarnings("all")
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
        id = getArguments().getInt(EXTRA_ID, 0);
    }

    protected abstract void fetchData();

    @SuppressWarnings("all")
    protected Client getClient() {
        return ((Application) getActivity().getApplication()).client;
    }

    @SuppressWarnings("unused")
    protected LinearLayoutManager getLayoutManager() {
        return new LinearLayoutManager(getApplicationContext());
    }

    @SuppressWarnings("all")
    protected Context getApplicationContext() {
        return getActivity().getApplicationContext();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
        disposables.dispose();
    }
}