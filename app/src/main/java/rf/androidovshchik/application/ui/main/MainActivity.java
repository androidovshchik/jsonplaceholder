package rf.androidovshchik.application.ui.main;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import rf.androidovshchik.application.R;
import rf.androidovshchik.application.models.user.User;
import rf.androidovshchik.application.ui.base.BaseActivity;
import timber.log.Timber;

public class MainActivity extends BaseActivity<UsersAdapter> {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.title_main);
        ButterKnife.bind(this);
        LinearLayoutManager layoutManager = getLayoutManager();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),
                layoutManager.getOrientation()));
        recyclerView.setAdapter(adapter);
        fetchData();
    }

    @Override
    protected void fetchData() {
        disposables.add(getClient().getUsers()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new UsersSubscriber()));
    }

    private class UsersSubscriber extends DisposableObserver<ArrayList<User>> {

        @Override
        public void onNext(ArrayList<User> users) {
            adapter.setItems(users);
        }

        @Override
        public void onComplete() {}

        @Override
        public void onError(Throwable e) {
            Timber.e(e);
        }
    }
}
