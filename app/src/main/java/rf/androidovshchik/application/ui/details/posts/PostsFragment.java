package rf.androidovshchik.application.ui.details.posts;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import rf.androidovshchik.application.R;
import rf.androidovshchik.application.models.Post;
import rf.androidovshchik.application.ui.base.BaseFragment;
import timber.log.Timber;

public class PostsFragment extends BaseFragment<PostsAdapter> {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public static PostsFragment newInstance(int id) {
        PostsFragment fragment = new PostsFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_posts, container, false);
        unbinder = ButterKnife.bind(this, view);
        LinearLayoutManager layoutManager = getLayoutManager();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),
                layoutManager.getOrientation()));
        recyclerView.setAdapter(adapter);
        fetchData();
        return view;
    }

    @Override
    protected void fetchData() {
        disposables.add(getClient().getPosts(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new PostsSubscriber()));
    }

    @SuppressWarnings("all")
    @OnClick(R.id.fab)
    void onFab() {
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setNegativeButton(getString(android.R.string.cancel), null)
                .setPositiveButton(getString(android.R.string.ok), null)
                .create();
        alertDialog.setTitle(R.string.dialog_message);
        alertDialog.setView(View.inflate(getApplicationContext(), R.layout.dialog_post, null));
        alertDialog.setOnShowListener((DialogInterface dialogInterface) -> {
            Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener((View view) -> {
                String title = ((EditText) alertDialog.findViewById(R.id.title)).getText()
                        .toString().trim();
                String body = ((EditText) alertDialog.findViewById(R.id.body)).getText()
                        .toString().trim();
                if (title.isEmpty() || body.isEmpty()) {
                    return;
                }
                Post post = new Post();
                post.title = title;
                post.body = body;
                adapter.addItem(post);
                recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
                alertDialog.cancel();
            });
        });
        alertDialog.show();
    }

    private class PostsSubscriber extends DisposableObserver<ArrayList<Post>> {

        @Override
        public void onNext(ArrayList<Post> posts) {
            adapter.setItems(posts);
        }

        @Override
        public void onComplete() {}

        @Override
        public void onError(Throwable e) {
            Timber.e(e);
        }
    }
}
