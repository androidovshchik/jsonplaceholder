package rf.androidovshchik.application.ui.details.albums;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.OnClick;
import rf.androidovshchik.application.models.Album;
import rf.androidovshchik.application.ui.base.BaseActivity;
import rf.androidovshchik.application.ui.base.BaseAdapter;
import rf.androidovshchik.application.ui.base.BaseViewHolder;
import rf.androidovshchik.application.ui.photos.PhotosActivity;
import rf.androidovshchik.application.utils.ViewUtil;

public class AlbumsAdapter extends BaseAdapter<Album, AlbumsAdapter.AlbumsViewHolder> {

    @Inject
    public AlbumsAdapter() {
        items = new ArrayList<>();
    }

    @Override
    public AlbumsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int padding = ViewUtil.dp2px(16);
        TextView textView = new TextView(parent.getContext().getApplicationContext());
        textView.setPadding(padding, padding, padding, padding);
        return new AlbumsViewHolder(textView);
    }

    @Override
    public void onBindViewHolder(AlbumsViewHolder holder, int position) {
        ((TextView) holder.itemView).setText(items.get(position).title);
    }

    public class AlbumsViewHolder extends BaseViewHolder {

        public AlbumsViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @OnClick(ID)
        void onUser() {
            Intent intent = new Intent(getApplicationContext(), PhotosActivity.class);
            intent.putExtra(BaseActivity.EXTRA_ID, items.get(getAdapterPosition()).id);
            getApplicationContext().startActivity(intent);
        }
    }
}
