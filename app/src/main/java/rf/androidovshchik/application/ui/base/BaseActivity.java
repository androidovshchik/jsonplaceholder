package rf.androidovshchik.application.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import io.reactivex.disposables.CompositeDisposable;
import rf.androidovshchik.application.Application;
import rf.androidovshchik.application.remote.Client;

@SuppressWarnings("Registered")
public abstract class BaseActivity<T extends BaseAdapter> extends AppCompatActivity {

    public static final String EXTRA_OBJECT = "object";
    public static final String EXTRA_ID = "id";

    protected int id;

    @Inject
    protected T adapter;

    protected CompositeDisposable disposables = new CompositeDisposable();

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        id = getIntent().getIntExtra(EXTRA_ID, 0);
    }

    protected abstract void fetchData();

    protected Client getClient() {
        return ((Application) getApplication()).client;
    }

    @SuppressWarnings("unused")
    protected LinearLayoutManager getLayoutManager() {
        return new LinearLayoutManager(getApplicationContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposables.dispose();
    }
}
