package rf.androidovshchik.application.ui.base;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public class BaseViewHolder extends RecyclerView.ViewHolder {

    protected static final int ID = 1;

    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
        itemView.setId(ID);
        ButterKnife.bind(this, itemView);
    }

    @SuppressWarnings("unused")
    public Context getApplicationContext() {
        return itemView.getContext().getApplicationContext();
    }
}