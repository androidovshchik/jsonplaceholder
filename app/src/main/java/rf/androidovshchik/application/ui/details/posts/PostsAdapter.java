package rf.androidovshchik.application.ui.details.posts;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import rf.androidovshchik.application.R;
import rf.androidovshchik.application.models.Post;
import rf.androidovshchik.application.ui.base.BaseAdapter;
import rf.androidovshchik.application.ui.base.BaseViewHolder;

public class PostsAdapter extends BaseAdapter<Post, PostsAdapter.PostsViewHolder> {

    @Inject
    public PostsAdapter() {
        items = new ArrayList<>();
    }

    public void addItem(Post item) {
        items.add(item);
        notifyDataSetChanged();
    }

    @Override
    public PostsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post,
                parent, false);
        return new PostsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostsViewHolder holder, int position) {
        holder.title.setText(items.get(position).title);
        holder.body.setText(items.get(position).body);
    }

    public class PostsViewHolder extends BaseViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.body)
        TextView body;

        public PostsViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
