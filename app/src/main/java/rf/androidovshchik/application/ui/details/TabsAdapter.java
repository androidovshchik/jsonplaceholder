package rf.androidovshchik.application.ui.details;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import rf.androidovshchik.application.R;

public class TabsAdapter extends FragmentStatePagerAdapter {

    public final List<Fragment> fragments;

    private final List<String> titles;

    @Inject
    public TabsAdapter(FragmentManager fragmentManager, Context context) {
        super(fragmentManager);
        fragments = new ArrayList<>();
        titles = Arrays.asList(context.getResources().getStringArray(R.array.tabs_titles));
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }
}