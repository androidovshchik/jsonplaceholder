package rf.androidovshchik.application.ui.details;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import rf.androidovshchik.application.R;
import rf.androidovshchik.application.models.user.User;
import rf.androidovshchik.application.ui.base.BaseActivity;
import rf.androidovshchik.application.ui.details.albums.AlbumsFragment;
import rf.androidovshchik.application.ui.details.posts.PostsFragment;
import rf.androidovshchik.application.ui.main.UsersAdapter;

public class DetailsActivity extends BaseActivity<UsersAdapter>
        implements HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentInjector;

    @BindView(R.id.details)
    TextView details;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @Inject
    TabsAdapter tabsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setTitle(R.string.title_details);
        ButterKnife.bind(this);
        User user = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_OBJECT));
        details.setText(user.getFullInfo(User.class));
        tabsAdapter.fragments.add(AlbumsFragment.newInstance(id));
        tabsAdapter.fragments.add(PostsFragment.newInstance(id));
        viewPager.setAdapter(tabsAdapter);
        tabs.setupWithViewPager(viewPager);
    }

    @Override
    protected void fetchData() {}

    @Override
    public final AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentInjector;
    }
}
