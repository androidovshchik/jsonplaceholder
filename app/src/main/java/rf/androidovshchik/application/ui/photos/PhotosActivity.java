package rf.androidovshchik.application.ui.photos;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import rf.androidovshchik.application.R;
import rf.androidovshchik.application.models.Photo;
import rf.androidovshchik.application.ui.base.BaseActivity;
import timber.log.Timber;

public class PhotosActivity extends BaseActivity<PhotosAdapter> {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);
        setTitle(R.string.title_photos);
        ButterKnife.bind(this);
        LinearLayoutManager layoutManager = getLayoutManager();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        fetchData();
    }

    @Override
    protected void fetchData() {
        disposables.add(getClient().getPhotos(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new PhotosSubscriber()));
    }

    private class PhotosSubscriber extends DisposableObserver<ArrayList<Photo>> {

        @Override
        public void onNext(ArrayList<Photo> users) {
            adapter.setItems(users);
        }

        @Override
        public void onComplete() {}

        @Override
        public void onError(Throwable e) {
            Timber.e(e);
        }
    }
}
