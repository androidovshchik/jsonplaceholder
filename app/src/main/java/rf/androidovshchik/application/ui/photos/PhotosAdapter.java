package rf.androidovshchik.application.ui.photos;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import rf.androidovshchik.application.R;
import rf.androidovshchik.application.models.Photo;
import rf.androidovshchik.application.ui.base.BaseAdapter;
import rf.androidovshchik.application.ui.base.BaseViewHolder;

public class PhotosAdapter extends BaseAdapter<Photo, PhotosAdapter.PhotosViewHolder> {

    @Inject
    public PhotosAdapter() {
        items = new ArrayList<>();
    }

    @Override
    public PhotosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo,
                parent, false);
        return new PhotosViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PhotosViewHolder holder, int position) {
        Glide.with(holder.getApplicationContext())
                .load(items.get(position).url)
                .into(holder.image);
        holder.title.setText(items.get(position).title);
    }

    public class PhotosViewHolder extends BaseViewHolder {

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.title)
        TextView title;

        public PhotosViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
