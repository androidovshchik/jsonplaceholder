package rf.androidovshchik.application.ui.main;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.OnClick;
import rf.androidovshchik.application.models.user.User;
import rf.androidovshchik.application.ui.base.BaseActivity;
import rf.androidovshchik.application.ui.base.BaseAdapter;
import rf.androidovshchik.application.ui.base.BaseViewHolder;
import rf.androidovshchik.application.ui.details.DetailsActivity;
import rf.androidovshchik.application.utils.ViewUtil;

public class UsersAdapter extends BaseAdapter<User, UsersAdapter.UsersViewHolder> {

    @Inject
    public UsersAdapter() {
        items = new ArrayList<>();
    }

    @Override
    public UsersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int padding = ViewUtil.dp2px(16);
        TextView textView = new TextView(parent.getContext().getApplicationContext());
        textView.setPadding(padding, padding, padding, padding);
        return new UsersViewHolder(textView);
    }

    @Override
    public void onBindViewHolder(UsersViewHolder holder, int position) {
        ((TextView) holder.itemView).setText(items.get(position).getPartInfo());
    }

    public class UsersViewHolder extends BaseViewHolder {

        public UsersViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @OnClick(ID)
        void onUser() {
            Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
            intent.putExtra(BaseActivity.EXTRA_OBJECT,
                    Parcels.wrap(items.get(getAdapterPosition())));
            intent.putExtra(BaseActivity.EXTRA_ID, items.get(getAdapterPosition()).id);
            getApplicationContext().startActivity(intent);
        }
    }
}
