package rf.androidovshchik.application.di.modules;

import android.support.v4.app.FragmentManager;

import dagger.Module;
import dagger.Provides;
import rf.androidovshchik.application.ui.details.DetailsActivity;

@Module
public class DetailsModule {

    @Provides
    FragmentManager provideFragmentManager(DetailsActivity activity) {
        return activity.getSupportFragmentManager();
    }
}