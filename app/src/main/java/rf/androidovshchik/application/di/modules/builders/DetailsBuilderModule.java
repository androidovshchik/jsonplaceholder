package rf.androidovshchik.application.di.modules.builders;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import rf.androidovshchik.application.di.scopes.PerFragment;
import rf.androidovshchik.application.ui.details.albums.AlbumsFragment;
import rf.androidovshchik.application.ui.details.posts.PostsFragment;

@SuppressWarnings("unused")
@Module(includes = AndroidSupportInjectionModule.class)
public interface DetailsBuilderModule {

    @PerFragment
    @ContributesAndroidInjector
    AlbumsFragment albumsInjector();

    @PerFragment
    @ContributesAndroidInjector
    PostsFragment postsInjector();
}