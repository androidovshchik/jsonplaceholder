package rf.androidovshchik.application.di.modules.builders;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import rf.androidovshchik.application.di.modules.DetailsModule;
import rf.androidovshchik.application.di.scopes.PerActivity;
import rf.androidovshchik.application.ui.photos.PhotosActivity;
import rf.androidovshchik.application.ui.details.DetailsActivity;
import rf.androidovshchik.application.ui.main.MainActivity;

@SuppressWarnings("unused")
@Module(includes = AndroidSupportInjectionModule.class)
public interface AppBuilderModule {

    @PerActivity
    @ContributesAndroidInjector
    MainActivity mainInjector();

    @PerActivity
    @ContributesAndroidInjector(modules = {
            DetailsModule.class,
            DetailsBuilderModule.class
    })
    DetailsActivity detailsInjector();

    @PerActivity
    @ContributesAndroidInjector
    PhotosActivity photosInjector();
}
