package rf.androidovshchik.application.di.components;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import rf.androidovshchik.application.Application;
import rf.androidovshchik.application.di.modules.AppModule;
import rf.androidovshchik.application.di.modules.builders.AppBuilderModule;

@Singleton
@Component(modules = {
        AppModule.class,
        AppBuilderModule.class
})
public interface AppComponent extends AndroidInjector<Application> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<Application> {}
}
