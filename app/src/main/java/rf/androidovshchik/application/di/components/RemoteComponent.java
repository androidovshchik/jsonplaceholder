package rf.androidovshchik.application.di.components;

import javax.inject.Singleton;

import dagger.Component;
import rf.androidovshchik.application.di.modules.RemoteModule;
import rf.androidovshchik.application.remote.Client;

@Singleton
@Component(modules = RemoteModule.class)
public interface RemoteComponent {

    Client client();
}
