package rf.androidovshchik.application.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import rf.androidovshchik.application.Application;

@Module
public class AppModule {

    @Provides
    Context provideContext(Application application) {
        return application.getApplicationContext();
    }
}
